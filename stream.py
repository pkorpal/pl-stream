from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream

import json
import requests
import datetime
import credentials
import logging
import datetime
import time

api_url = "http://91.192.224.157/api/"
logger = logging.getLogger("stream")
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s:%(levelname)s: %(message)s")
handler = logging.FileHandler('stream.log')
handler.setLevel(logging.DEBUG)
handler.setFormatter(formatter)
logger.addHandler(handler)
base_url = "http://127.0.0.1:9200/"
troll_es = "http://91.192.224.158:9200/"
twitter_accounts_index = "twitter-accounts"
tweets_index = "tweets"
headers = {"Content-Type" : "application/json"}

class StdOutListener(StreamListener):

    def __init__(self):
        pass

    def on_data(self, data):
        print("New tweet")
        tweet = json.loads(data)
        # print(json.dumps(tweet, indent=2))
        try:
            add_tweet_to_index(tweet)
            return True
        except:
            return True
    
    def on_error(self, status):
        print(f"Stream error {status}")

def add_tweet_to_index(data):
    print("Adding tweet to index")
    user = data.get("user")
    quoted_status = data.get("quoted_status", None)
    retweeted_status = data.get("retweeted_status", None)
    is_retweet = False
    retweet_id = None
    author_username_of_retweet = None
    author_name_of_retweet = None
    retweet_text = None
  
    print("Checking if tweet is a quoted_status")
    if quoted_status is not None:
        quoted_user = quoted_status.get("user")
        is_retweet = True
        retweet_id = data.get("quoted_status_id_str")
        author_username_of_retweet = quoted_user.get("screen_name")
        author_name_of_retweet = quoted_user.get("name")
        retweet_text = quoted_status.get("text")
    print("Checking if tweet is a retweet")
    if retweeted_status is not None:
        retweeted_user = retweeted_status.get("user")
        is_retweet = True
        retweet_id = retweeted_status.get("id_str")
        author_username_of_retweet = retweeted_user.get("screen_name")
        author_name_of_retweet = retweeted_user.get("name")
        retweet_text = retweeted_status.get("text")

    print("Creating timestamp")
    timestamp_ms = int(round(time.time() * 1000))

    try:
        avatar_url = user.get("profile_image_url_https", None)
        in_reply_to = data.get("in_reply_to_screen_name", None)
        in_reply_to_status_id = data.get("in_reply_to_status_id_str", None)
        tweet_id = data.get("id_str", None)
        author_username = user.get("screen_name", None)
        author_name = user.get("name", None)
        content = data.get("text", "")
        thread_count = data.get("reply_count", 0)
        retweet_count = data.get("retweet_count", 0)
        reacts = data.get("favorite_count", 0)
        posted_utime = str(timestamp_ms)
        data_to_save = {
            "avatar_url" : avatar_url,
            "in_reply_to" : in_reply_to,
            "in_reply_to_tweet_id" : in_reply_to_status_id, 
            "is_retweet" : is_retweet,
            "tweet_id" : tweet_id,
            "retweet_id" : retweet_id,
            "author_username" : author_username,
            "author_name" : author_name,
            "author_username_of_retweet" : author_username_of_retweet,
            "author_name_of_retweet" : author_name_of_retweet,
            "content" : content,
            "retweet_text" : retweet_text,
            "thread_count" : thread_count,
            "retweet_count" : retweet_count,
            "reacts" : reacts,
            "posted_utime" : posted_utime,
            "imgs" : []
        }
        data_json = json.dumps(data_to_save)
        print(f"Created json to save {json.dumps(data_to_save, indent=2)}")
        url = base_url + tweets_index + '/tweet'
        print("Adding new tweet to index")
        r = requests.post(url, data=data_json, headers=headers)
        # time.sleep(r.elapsed.total_seconds())
        print(f"Elasticsearch response {json.dumps(r.json(), indent=2)}")
    except Exception as e:
        tweet_id = data.get("id_str", None)
        print(f"Error while adding tweet_id {tweet_id} to index {e}")

def get_ids_to_follow():
  print("Getting accounts to follow")
  list_of_usernames = []
  url = troll_es + twitter_accounts_index + "/_search"
  body = {
    "query" : {
      "match" : {
        "observe" : True
      }
    },
    "size" : 5000
  }
  body_json = json.dumps(body)
  try:
    r = requests.get(url, data=body_json, headers=headers)
    print(f"Elasticsearch response received")
    response = json.loads(r.text)
    hits = response.get("hits")
    dict_hits = hits.get("hits")
    for twitter_account in dict_hits:
        source = twitter_account.get("_source")
        list_of_usernames.append(str(source.get("id")))
    print(f"Number of accounts to follow {len(list_of_usernames)}")
    print(f"Accounts to folor {list_of_usernames}")
  except Exception as e:
      print(e)
  return list_of_usernames

def get_tags_to_follow():
  url = api_url + "tags/" + str(9999)
  r = requests.get(url)
  print(f"get_tags_response {r.json()}")
  return r.json()

if __name__ == "__main__":
  while True:
    print("Starting stream")
    listener = StdOutListener()
    auth = OAuthHandler(credentials.API_KEY, credentials.API_SECRET_KEY)
    auth.set_access_token(credentials.ACCESS_TOKEN, credentials.ACCESS_SECRET_TOKEN)
    stream = Stream(auth, listener)
    try:
        ids = get_ids_to_follow()
        tags = get_tags_to_follow()
        pl_words = ["i", "lub", "oraz", "to", "po", "za", "albo", "tez", "na", "pod", "bo", "się", "w", "nie", "do"]
        track = []
        for tag in tags:
            track.append(tag)
        for pl_word in pl_words:
            track.append(pl_word)
        stream.filter(languages=["pl"], track=track, follow=ids)
    except:
        time.sleep(2)
  print("Ending stream")
